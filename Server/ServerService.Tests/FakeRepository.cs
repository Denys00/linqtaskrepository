﻿using Server.Abstractions;
using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerService.Tests
{
    class FakeRepository : ITeamRepository
    {
        public readonly List<Team> _teams;

        public FakeRepository()
        {
            _teams = new List<Team>()
            {
                new Team
                {
                    Id = 1,
                    Name = "labore",
                    Created_At = new DateTime(2019,6,19)
                },

                new Team
                {
                    Id = 2,
                    Name = "sunt",
                    Created_At = new DateTime(2019,6,19)
                }
            };
        }

        public async Task<Team> Add(Team team)
        {
            team.Id = _teams.Max(u => u.Id) + 1;
            _teams.Add(team);
            return team;
        }

        public async Task<Team> Delete(int id)
        {
            Team team = _teams.FirstOrDefault(u => u.Id == id);
            if (team != null)
            {
                _teams.Remove(team);
            }
            return team;
        }

        public async Task<Team> GetTeam(int id)
        {
            return _teams.FirstOrDefault(u => u.Id == id);
        }

        public async Task<IEnumerable<Team>> GetTeams()
        {
            return _teams;
        }

        public async Task<Team> Update(Team teamChanges)
        {
            Team team = _teams.FirstOrDefault(u => u.Id == teamChanges.Id);
            if (team != null)
            {
                team = Copy(teamChanges);
                int index = _teams.FindIndex(m => m.Id == teamChanges.Id);
                _teams[index] = team;
            }
            return team;
        }

        private Team Copy(Team model)
        {
            Team team = new Team();
            team.Id = model.Id;
            team.Name = model.Name;
            team.Created_At = model.Created_At;

            return team;
        }
    }
}
