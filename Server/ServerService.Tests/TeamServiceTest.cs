using Microsoft.VisualStudio.TestTools.UnitTesting;
using Server.Models;
using Server.Services;
using System;
using System.Threading.Tasks;

namespace ServerService.Tests
{
    [TestClass]
    public class TeamServiceTest
    {
        [TestMethod]
        public async Task When_Add_Team_Then_Count_Increase()
        {
            var repo = new FakeRepository();
            var service = new TeamService(repo);
            var count = repo._teams.Count;

            var result = await service.Add(new Team { Name = "Test", Created_At = DateTime.Now });

            Assert.AreEqual(count + 1, repo._teams.Count);
        }

        [TestMethod]
        public async Task When_Update_Task_State_Then_Update_It()
        {
            var repo = new FakeTaskRepository();
            var service = new TaskService(repo);
            var task = new TaskModel
            {
                Id = 1,
                Name = "One Task Larger than 45 5 5 5 5 5 5 5 5 5 5 aksdnas nda sdkasn dladsn als ndan sd sdnk aldn las dnlas dal nd aknd w wakndaw ld  laksn d na",
                Created_At = new DateTime(2019, 6, 19),
                Description = "Inventore quae alias magnam sed qui sint",
                Finished_At = new DateTime(2020, 2, 1),
                Performer_id = 4,
                Project_id = 1,
                State = 3
            };

            var result = await service.Update(task);

            Assert.IsTrue(repo._tasks.Find(p => p.Id == 1).State == 3);
        }

        [TestMethod]
        public async Task When_Get_Not_Existing_Task_Then_Null_Result()
        {
            var repo = new FakeTaskRepository();
            var service = new TaskService(repo);

            var result = await service.Get(5);

            Assert.IsNull(result);
        }

        [TestMethod]
        public async Task When_Get_All_Tasks_Then_Not_Null()
        {
            var repo = new FakeTaskRepository();
            var service = new TaskService(repo);

            var result = await service.GetAll();
             
            Assert.IsNotNull(result);
        }
    }
}
