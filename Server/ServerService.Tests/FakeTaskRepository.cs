﻿using Server.Abstractions;
using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerService.Tests
{
    class FakeTaskRepository : ITaskRepository
    {
        public List<TaskModel> _tasks;

        public FakeTaskRepository()
        {
            _tasks = new List<TaskModel>
            {
                new TaskModel
                {
                    Id = 1,
                    Name = "One Task Larger than 45 5 5 5 5 5 5 5 5 5 5 aksdnas nda sdkasn dladsn als ndan sd sdnk aldn las dnlas dal nd aknd w wakndaw ld  laksn d na",
                    Created_At = new DateTime(2019, 6, 19),
                    Description = "Inventore quae alias magnam sed qui sint",
                    Finished_At = new DateTime(2020,2,1),
                    Performer_id = 4,
                    Project_id = 1,
                    State = 1
                },
                new TaskModel
                {
                    Id = 2,
                    Name = "KDAMSld adsm alskd",
                    Created_At = new DateTime(2019, 6, 19),
                    Description = "Similique molestiae esse eius nihil repudiandae possimus eos ea nobis.\nEaque ipsam atque sequi dignissimos fugiat voluptate hic nihil.",
                    Finished_At = new DateTime(2019,8,20),
                    Performer_id = 2,
                    Project_id = 1,
                    State = 3
                },
                new TaskModel
                {
                    Id = 3,
                    Name = "dasd madp aksd apsp dsa",
                    Created_At = new DateTime(2019, 5, 19),
                    Description = "ihil repudiandae possimus eos ea nobis.\nEaque ipsam atque sequi dignissimos",
                    Finished_At = new DateTime(2020,12,1),
                    Performer_id = 3,
                    Project_id = 1,
                    State = 4
                },
                new TaskModel
                {
                    Id = 4,
                    Name = "131 312 4 014014 -1-4021412312 ",
                    Created_At = new DateTime(2019, 8, 19),
                    Description = "quae alias",
                    Finished_At = new DateTime(2019, 12,1),
                    Performer_id = 1,
                    Project_id = 1,
                    State = 2
                }
            };
        }

        public async Task<TaskModel> Add(TaskModel task)
        {
            task.Id = _tasks.Max(u => u.Id) + 1;
            _tasks.Add(task);
            return task;
        }

        public async Task<TaskModel> Delete(int id)
        {
            TaskModel task = _tasks.FirstOrDefault(u => u.Id == id);
            if (task != null)
            {
                _tasks.Remove(task);
            }
            return task;
        }

        public async Task<TaskModel> GetTask(int id)
        {
            return _tasks.FirstOrDefault(u => u.Id == id);
        }

        public async Task<IEnumerable<TaskModel>> GetTasks()
        {
            return _tasks;
        }

        public async Task<TaskModel> Update(TaskModel teamChanges)
        {
            TaskModel task = _tasks.FirstOrDefault(u => u.Id == teamChanges.Id);
            if (task != null)
            {
                task = Copy(teamChanges);
                int index = _tasks.FindIndex(m => m.Id == teamChanges.Id);
                _tasks[index] = task;
            }
            return task;
        }

        private TaskModel Copy(TaskModel model)
        {
            TaskModel task = new TaskModel();
            task.Id = model.Id;
            task.Name = model.Name;
            task.Created_At = model.Created_At;
            task.Description = model.Description;
            task.Finished_At = model.Finished_At;
            task.Performer_id = model.Performer_id;
            task.Project_id = model.Project_id;
            task.State = model.State;


            return task;
        }
    
    }
}
