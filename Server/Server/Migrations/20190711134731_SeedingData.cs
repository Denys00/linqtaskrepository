﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Server.Migrations
{
    public partial class SeedingData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Users_UserId",
                table: "Projects");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskModels_Projects_ProjectId",
                table: "TaskModels");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskModels_TaskStates_TaskStateId",
                table: "TaskModels");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskModels_Users_UserId",
                table: "TaskModels");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Teams_TeamId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_TeamId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_TaskModels_ProjectId",
                table: "TaskModels");

            migrationBuilder.DropIndex(
                name: "IX_TaskModels_TaskStateId",
                table: "TaskModels");

            migrationBuilder.DropIndex(
                name: "IX_TaskModels_UserId",
                table: "TaskModels");

            migrationBuilder.DropIndex(
                name: "IX_Projects_TeamId",
                table: "Projects");

            migrationBuilder.DropIndex(
                name: "IX_Projects_UserId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "TeamId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "TaskModels");

            migrationBuilder.DropColumn(
                name: "TaskStateId",
                table: "TaskModels");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "TaskModels");

            migrationBuilder.DropColumn(
                name: "TeamId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Projects");

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "Author_id", "Created_At", "Deadline", "Description", "Name", "Team_id" },
                values: new object[] { 1, 1, new DateTime(2019, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2020, 9, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "is a 1977 American epic space-opera film written and directed by George Lucas. It is the first film in the original Star Wars trilogy and the beginning of the Star Wars franchise. ", "The Coolest Project in da world", 2 });

            migrationBuilder.InsertData(
                table: "TaskModels",
                columns: new[] { "Id", "Created_At", "Description", "Finished_At", "Name", "Performer_id", "Project_id", "State" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Inventore quae alias magnam sed qui sint", new DateTime(2020, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "One Task Larger than 45 5 5 5 5 5 5 5 5 5 5 aksdnas nda sdkasn dladsn als ndan sd sdnk aldn las dnlas dal nd aknd w wakndaw ld  laksn d na", 4, 1, 1 },
                    { 2, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Similique molestiae esse eius nihil repudiandae possimus eos ea nobis.
                Eaque ipsam atque sequi dignissimos fugiat voluptate hic nihil.", new DateTime(2019, 8, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "KDAMSld adsm alskd", 2, 1, 3 },
                    { 3, new DateTime(2019, 5, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), @"ihil repudiandae possimus eos ea nobis.
                Eaque ipsam atque sequi dignissimos", new DateTime(2020, 12, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "dasd madp aksd apsp dsa", 3, 1, 4 },
                    { 4, new DateTime(2019, 8, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "quae alias", new DateTime(2019, 12, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "131 312 4 014014 -1-4021412312 ", 1, 1, 2 }
                });

            migrationBuilder.InsertData(
                table: "TaskStates",
                columns: new[] { "Id", "Value" },
                values: new object[,]
                {
                    { 1, "Created" },
                    { 2, "Started" },
                    { 3, "Finished" },
                    { 4, "Canceled" }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "Created_At", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "labore" },
                    { 2, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "sunt" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "First_Name", "Last_Name", "Registered_At", "Team_Id" },
                values: new object[,]
                {
                    { 1, new DateTime(2013, 11, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Gayle_Swift98@yahoo.com", "Gayle", "Swift", new DateTime(2019, 6, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 },
                    { 2, new DateTime(2017, 10, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tad_Powlowski27@hotmail.com", "Tad", "Polowski", new DateTime(2019, 6, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 },
                    { 3, new DateTime(2002, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Letha.Strosin58@gmail.com", "Letha", "Strosin", new DateTime(2019, 6, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 },
                    { 4, new DateTime(2005, 5, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Letha@gmail.com", "Ivan", "Many", new DateTime(2019, 2, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TaskModels",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TaskModels",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TaskModels",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "TaskModels",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "TaskStates",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.AddColumn<int>(
                name: "TeamId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "TaskModels",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TaskStateId",
                table: "TaskModels",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "TaskModels",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeamId",
                table: "Projects",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Projects",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskModels_ProjectId",
                table: "TaskModels",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskModels_TaskStateId",
                table: "TaskModels",
                column: "TaskStateId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskModels_UserId",
                table: "TaskModels",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_UserId",
                table: "Projects",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Teams_TeamId",
                table: "Projects",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Users_UserId",
                table: "Projects",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskModels_Projects_ProjectId",
                table: "TaskModels",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskModels_TaskStates_TaskStateId",
                table: "TaskModels",
                column: "TaskStateId",
                principalTable: "TaskStates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskModels_Users_UserId",
                table: "TaskModels",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Teams_TeamId",
                table: "Users",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
