﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Server.Abstractions;
using Server.Models;
using Server.Services;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private IService<TaskModel> _repository;
        private IQueueService _queueService;
        public TasksController(IService<TaskModel> repository, IQueueService queueService)
        {
            _repository = repository;
            _queueService = queueService;
        }

        // GET: api/Task
        [HttpGet]
        public async Task<IEnumerable<TaskModel>> Get()
        {
            await Task.Run(() =>_queueService.PostValue("Tasks Get Was Triggered"));
            return await _repository.GetAll();
        }

        // GET: api/Task/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<TaskModel> Get(int id)
        {
            await Task.Run(() => _queueService.PostValue("Task Get Was Triggered"));
            return await _repository.Get(id);
        }

        // POST: api/Task
        [HttpPost]
        public async Task Post([FromBody] TaskModel value)
        {
            await Task.Run(() => _queueService.PostValue("Task Post Was Triggered"));
            await _repository.Add(value);
        }

        // PUT: api/Task/5
        [HttpPut]
        public async Task Put([FromBody] TaskModel value)
        {
            await Task.Run(() => _queueService.PostValue("Task Put Was Triggered"));
            await _repository.Update(value);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await Task.Run(() => _queueService.PostValue("Task Delete Was Triggered"));
            await _repository.Delete(id);
        }
    }
}
