﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Server.Abstractions;
using Server.Models;
using Server.Services;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private IService<Team> _service;
        private IQueueService _queueService;

        public TeamsController(IService<Team> service, IQueueService queueService)
        {
            _queueService = queueService;
            _service = service;
        }

        // GET: api/Team
        [HttpGet]
        public async Task<IEnumerable<Team>> Get()
        {
            await Task.Run(() => _queueService.PostValue("Teams Get Was Triggered"));
            return await _service.GetAll();
        }

        // GET: api/Team/5
        [HttpGet("{id}", Name = "GetTeams")]
        public async Task<Team> Get(int id)
        {
            await Task.Run(() => _queueService.PostValue("Team Get Was Triggered"));
            return await _service.Get(id);
        }

        // POST: api/Team
        [HttpPost]
        public async Task Post([FromBody] Team value)
        {
            await Task.Run(() => _queueService.PostValue("Team Post Was Triggered"));
            await _service.Add(value);
        }

        // PUT: api/Team/5
        [HttpPut]
        public async Task Put([FromBody] Team value)
        {
            await Task.Run(() => _queueService.PostValue("Team Put Was Triggered"));
            await _service.Update(value);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await Task.Run(() => _queueService.PostValue("Team Delete Was Triggered"));
            await _service.Delete(id);
        }
    }
}
