﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Server.Abstractions;
using Server.Models;
using Server.Services;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserRepository _repository;
        private IQueueService _queueService;

        public UsersController(IUserRepository repository, IQueueService queueService)
        {
            _queueService = queueService;
            _repository = repository;
        }

        // GET: api/User
        [HttpGet]
        public async Task<IEnumerable<User>> Get()
        {
            await Task.Run(() => _queueService.PostValue("Users Get Was Triggered"));
            return await _repository.GetUsers();
        }

        // GET: api/User/5
        [HttpGet("{id}", Name = "GetUsers")]
        public async Task<User> Get(int id)
        {
            await Task.Run(() => _queueService.PostValue("User Get Was Triggered"));
            return await _repository.GetUser(id);
        }

        // POST: api/User
        [HttpPost]
        public async Task Post([FromBody] User value)
        {
            await Task.Run(() => _queueService.PostValue("User Post Was Triggered"));
            await _repository.Add(value);
        }

        // PUT: api/User/5
        [HttpPut]
        public async Task Put([FromBody] User value)
        {
            await Task.Run(() => _queueService.PostValue("User Put Was Triggered"));
            await _repository.Update(value);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<User> Delete(int id)
        {
            await Task.Run(() => _queueService.PostValue("User Delete Was Triggered"));
            return await _repository.Delete(id);
        }
    }
}
