﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Server.Abstractions;
using Server.Models;
using Server.Services;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        private ITaskStateRepository _repository;
        private IQueueService _queueService;

        public TaskStatesController(ITaskStateRepository repository, IQueueService queueService)
        {
            _queueService = queueService;
            _repository = repository;
        }

        // GET: api/TaskState
        [HttpGet]
        public async Task<IEnumerable<TaskState>> Get()
        {
            await Task.Run(() => _queueService.PostValue("Task States Get Was Triggered"));
            return  await _repository.GetTaskStates();
        }

        // GET: api/TaskState/5
        [HttpGet("{id}", Name = "GetTeskStates")]
        public async Task<TaskState> Get(int id)
        {
            await Task.Run(() => _queueService.PostValue("Task State Get Was Triggered"));
            return await _repository.GetTaskState(id);
        }

        // POST: api/TaskState
        [HttpPost]
        public async Task Post([FromBody] TaskState value)
        {
            await Task.Run(() => _queueService.PostValue("Task State Post Was Triggered"));
            await _repository.Add(value);
        }

        // PUT: api/TaskState/5
        [HttpPut]
        public async Task Put([FromBody] TaskState value)
        {
            await Task.Run(() => _queueService.PostValue("Task State Put Was Triggered"));
            await _repository.Update(value);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await Task.Run(() => _queueService.PostValue("Task State Delete Was Triggered"));
            await _repository.Delete(id);
        }
    }
}
