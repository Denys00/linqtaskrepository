﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Server.Abstractions;
using Server.Models;
using Server.Services;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        public IProjectRepository _repository;
        private IQueueService _queueService;

        public ProjectsController(IProjectRepository repository, IQueueService queueService)
        {
            _repository = repository;
            _queueService = queueService;
        }

        // GET: api/Project
        [HttpGet]
        public async Task<IEnumerable<Project>> Get()
        {
            await Task.Run(() => _queueService.PostValue("Projects Get Was Triggered"));
            return await _repository.GetProjects();
        }

        // GET: api/Project/5
        [HttpGet("{id}", Name = "GetProjects")]
        public async Task<Project> Get(int id)
        {
            await Task.Run(() =>_queueService.PostValue("Project Get Was Triggered"));
            return await _repository.GetProject(id);
        }

        // POST: api/Project
        [HttpPost]
        public async Task Post([FromBody] Project value)
        {
            await Task.Run(() =>_queueService.PostValue("Project Post Was Triggered"));
            await _repository.Add(value);
        }

        // PUT: api/Project/5
        [HttpPut]
        public async Task Put([FromBody] Project value)
        {
            await Task.Run(() =>_queueService.PostValue("Project Put Was Triggered"));
            await _repository.Update(value);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await Task.Run(() => _queueService.PostValue("Project Delete Was Triggered"));
            await _repository.Delete(id);
        }
    }
}
