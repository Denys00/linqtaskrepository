﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Models
{
    public class TaskState
    {
        public int Id { get; set; }
        [MaxLength(255)]
        public string Value { get; set; }
    }
}
