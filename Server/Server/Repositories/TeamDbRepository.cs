﻿using Microsoft.EntityFrameworkCore;
using Server.Abstractions;
using Server.Context;
using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Repositories
{
    public class TeamDbRepository : ITeamRepository
    {
        private IDbContext _context;

        public TeamDbRepository(IDbContext context)
        {
            _context = context;
        }

        public async Task<Team> Add(Team team)
        {
            await Task.Run(() =>
            {
                _context.Teams.Update(team);
                _context.SaveChanges();
            });
            return team;
        }

        public async Task<Team> Delete(int id)
        {
            return await Task.Run(() =>
            {
                var project = _context.Teams.FirstOrDefault(u => u.Id == id);
                if (project != null)
                {
                    _context.Teams.Remove(project);
                    _context.SaveChanges();
                }
                return project;
            });
        }

        public async Task<Team> GetTeam(int id)
        {
            return await _context.Teams.FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<IEnumerable<Team>> GetTeams()
        {
            return await _context.Teams.ToListAsync();
        }

        public async Task<Team> Update(Team team)
        {
            await Task.Run(() =>
            {
                _context.Teams.Update(team);
                _context.SaveChanges();
            });
            return team;
        }
    }
}
