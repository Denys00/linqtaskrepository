﻿using Microsoft.EntityFrameworkCore;
using Server.Abstractions;
using Server.Context;
using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Repositories
{
    public class ProjectDbRepository : IProjectRepository
    {

        private IDbContext _context;

        public ProjectDbRepository(IDbContext context)
        {
            _context = context;
        }

        public async Task<Project> Add(Project project)
        {
            await Task.Run(() =>
            {
                _context.Projects.Update(project);
                _context.SaveChanges();
            });
            return project;
        }

        public async Task<Project> Delete(int id)
        {
            var project = await _context.Projects.FirstOrDefaultAsync(u => u.Id == id);
            if (project != null)
            {
                _context.Projects.Remove(project);
                _context.SaveChanges();
            }
            return project;
        }

        public async Task<Project> GetProject(int id)
        {
            return await _context.Projects.FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<IEnumerable<Project>> GetProjects()
        {
            return await _context.Projects.ToListAsync();
        }

        public async Task<Project> Update(Project projectChanges)
        {
            await Task.Run(() =>
            {
                _context.Projects.Update(projectChanges);
                _context.SaveChanges();
            });
            return projectChanges;
        }

    }
}
