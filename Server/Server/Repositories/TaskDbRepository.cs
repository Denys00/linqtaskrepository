﻿using Server.Abstractions;
using Server.Context;
using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Repositories
{
    public class TaskDbRepository : ITaskRepository
    {
        private IDbContext _context;

        public TaskDbRepository(IDbContext context)
        {
            _context = context;
        }

        public async Task<TaskModel> Add(TaskModel project)
        {
            await Task.Run(() =>
            {
                _context.TaskModels.Update(project);
                _context.SaveChanges();
            });
            return project;
        }

        public async Task<TaskModel> Delete(int id)
        {
            var project = await Task.Run(() => _context.TaskModels.FirstOrDefault(u => u.Id == id));
            await Task.Run(() =>
            {
                if (project != null)
                {
                    _context.TaskModels.Remove(project);
                    _context.SaveChanges();
                }
            });
            return project;
        }

        public async Task<TaskModel> GetTask(int id)
        {
            return await Task.Run(() =>_context.TaskModels.FirstOrDefault(u => u.Id == id));
        }

        public async Task<IEnumerable<TaskModel>> GetTasks()
        {
            return await Task.Run(() => _context.TaskModels.ToList());
        }

        public async Task<TaskModel> Update(TaskModel projectChanges)
        {
            await Task.Run(() =>
            {
                _context.TaskModels.Update(projectChanges);
                _context.SaveChanges();
            });
            return projectChanges;
        }

    }
}

