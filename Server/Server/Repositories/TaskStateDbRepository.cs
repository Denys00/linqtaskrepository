﻿using Server.Abstractions;
using Server.Context;
using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Repositories
{
    public class TaskStateDbRepository : ITaskStateRepository
    {
        private IDbContext _context;

        public TaskStateDbRepository(IDbContext context)
        {
            _context = context;
        }

        public async Task<TaskState> Add(TaskState taskState)
        {
            await Task.Run(() =>
            {
                _context.TaskStates.Update(taskState);
                _context.SaveChanges();
            });
            return taskState;
        }

        public async Task<TaskState> Delete(int id)
        {
            var project = await Task.Run(() =>_context.TaskStates.FirstOrDefault(u => u.Id == id));
            if (project != null)
            {
                await Task.Run(() =>
                {
                    _context.TaskStates.Remove(project);
                    _context.SaveChanges();
                });
            }
            return project;
        }

        public async Task<TaskState> GetTaskState(int id)
        {
            return await Task.Run(() => _context.TaskStates.FirstOrDefault(u => u.Id == id));
        }

        public async Task<IEnumerable<TaskState>> GetTaskStates()
        {
            return await Task.Run(() =>_context.TaskStates.ToList());
        }

        public async Task<TaskState> Update(TaskState taskState)
        {
            await Task.Run(() =>
            {
                _context.TaskStates.Update(taskState);
                _context.SaveChanges();
            });
            return taskState;
        }
    }
}
