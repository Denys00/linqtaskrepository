﻿using Server.Abstractions;
using Server.Context;
using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Repositories
{
    public class UserDbRepository : IUserRepository
    {
        private IDbContext _context;

        public UserDbRepository(IDbContext context)
        {
            _context = context;
        }

        public async Task<User> Add(User user)
        {
            return await Task.Run(() =>
            {
                _context.Users.AddAsync(user);
                _context.SaveChanges();
                return user;
            });
        }

        public async Task<User> Delete(int id)
        {
            return await Task.Run(() =>
            {
                var project = _context.Users.FirstOrDefault(u => u.Id == id);
                if (project != null)
                {
                    _context.Users.Update(project);
                    _context.SaveChanges();
                }
                return project;
            });
        }

        public async Task<User> GetUser(int id)
        {
            return await Task.Run(() => _context.Users.FirstOrDefault(u => u.Id == id));
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await Task.Run(() => _context.Users.ToList());
        }

        public async Task<User> Update(User user)
        {
            return await Task.Run(() =>
            {
                _context.Users.Update(user);
                _context.SaveChanges();
                return user;
            });
        }
    }
}
