﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public static class FileWorkService
    {
        public static IEnumerable<string> GetMessage(string path)
        {
            return System.IO.File.ReadAllLines(path).ToList();
        }

        public static async  Task<string> GetSortedRequests(string path)
        {
            return await Task.Run(() =>
            {
                var lines = GetMessage(path);
                lines = lines.Reverse();
                string TextLine = "";
                foreach (var line in lines)
                {
                    TextLine += $"{line}\n";
                }
                return TextLine;
            });
        }


    }
}
