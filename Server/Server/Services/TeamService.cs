﻿using Server.Abstractions;
using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class TeamService : IService<Team>
    {
        private ITeamRepository _repository;

        public TeamService(ITeamRepository repository)
        {
            _repository = repository;
        }

        public async Task<Team> Add(Team taskState)
        {
            return await _repository.Add(taskState);
        }

        public async Task<Team> Delete(int id)
        {
            return await _repository.Delete(id);
        }

        public async Task<Team> Get(int id)
        {
            return await _repository.GetTeam(id);
        }

        public async Task<IEnumerable<Team>> GetAll()
        {
            return await _repository.GetTeams();
        }

        public async Task<Team> Update(Team taskState)
        {
            return await _repository.Update(taskState);
        }
    }
}
