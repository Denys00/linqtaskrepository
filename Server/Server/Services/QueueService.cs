﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Util;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Server.Abstractions;
using Server.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Services
{
    public class QueueService : IQueueService, IDisposable
    {
        private readonly IConnectionFactory _factory;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly IHubContext<MainHub> _hub;

        public QueueService(IHubContext<MainHub> hub)
        {
            _hub = hub;
            _factory = new ConnectionFactory()
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672")
            };
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            CreateConsumer();

        }

        private void CreateConsumer()
        {
            _channel.ExchangeDeclare("BackExchange", ExchangeType.Direct);
            _channel.QueueDeclare("BackQueue",
                durable: true,
                exclusive: false,
                autoDelete: false
                );

            _channel.QueueBind(queue: "BackQueue", exchange: "BackExchange", routingKey: "backKey");

            var _consumer = new EventingBasicConsumer(_channel);

            _consumer.Received += MessageRecived;

            _channel.BasicConsume(queue: "BackQueue",
                autoAck: false,
                consumer: _consumer);

        }

        public void MessageRecived(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body;
            var message = Encoding.UTF8.GetString(body);
            _hub.Clients.All.SendAsync("GetNotification", message);
            _channel.BasicAck(args.DeliveryTag, false);

        }

        public bool PostValue(string message)
        {
            _channel.ExchangeDeclare("Exchange", ExchangeType.Direct);

            var body = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(exchange: "Exchange",
                routingKey: "key",
                basicProperties: null,
                body: body);

            return true;
        }

        public void Dispose()
        {
            _connection?.Dispose();
            _channel?.Dispose();
        }

        
    }
}
