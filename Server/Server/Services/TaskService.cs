﻿using Server.Abstractions;
using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class TaskService : IService<TaskModel>
    {
        private ITaskRepository _repository;

        public TaskService(ITaskRepository repository)
        {
            _repository = repository;
        }

        public async Task<TaskModel> Add(TaskModel taskState)
        {
            return await _repository.Add(taskState);
        }

        public async Task<TaskModel> Delete(int id)
        {
            return await _repository.Delete(id);
        }

        public async Task<TaskModel> Get(int id)
        {
            return await _repository.GetTask(id);
        }

        public async Task<IEnumerable<TaskModel>> GetAll()
        {
            return await _repository.GetTasks();
        }

        public async Task<TaskModel> Update(TaskModel taskState)
        {
            return await _repository.Update(taskState);
        }
    }

}

