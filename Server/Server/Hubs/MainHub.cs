﻿using Microsoft.AspNetCore.SignalR;
using Server.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Hubs
{
    
    public class MainHub : Hub
    {
        public async Task GetSortedFile()
        {
            string message = await FileWorkService.GetSortedRequests(@"C:\MyProjects\binary-studio\linqtaskrepository\Worker\Worker\Info.txt");
            await Clients.All.SendAsync("GetNotification", message);
        }
    }
}
