﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Models;
using Server.Abstractions;

namespace Server.Context
{
    public class DormitoryDbContext : DbContext, IDbContext
    {
        public DormitoryDbContext(DbContextOptions<DormitoryDbContext> options) :
            base(options)
        { }

        public DbSet<Project> Projects { get; set; }
        public DbSet<TaskModel> TaskModels { get; set; }
        public DbSet<TaskState> TaskStates { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }
        
        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    Author_id = 1,
                    Created_At = new DateTime(2019,1,1),
                    Deadline = new DateTime(2020,9,12),
                    Description = "is a 1977 American epic space-opera film written and directed by George Lucas. It is the first film in the original Star Wars trilogy and the beginning of the Star Wars franchise. ",
                    Name = "The Coolest Project in da world",
                    Team_id = 2
                }
            };

            var tasks = new List<TaskModel>
            {
                new TaskModel
                {
                    Id = 1,
                    Name = "One Task Larger than 45 5 5 5 5 5 5 5 5 5 5 aksdnas nda sdkasn dladsn als ndan sd sdnk aldn las dnlas dal nd aknd w wakndaw ld  laksn d na",
                    Created_At = new DateTime(2019, 6, 19),
                    Description = "Inventore quae alias magnam sed qui sint",
                    Finished_At = new DateTime(2020,2,1),
                    Performer_id = 4,
                    Project_id = 1,
                    State = 1
                },
                new TaskModel
                {
                    Id = 2,
                    Name = "KDAMSld adsm alskd",
                    Created_At = new DateTime(2019, 6, 19),
                    Description = "Similique molestiae esse eius nihil repudiandae possimus eos ea nobis.\nEaque ipsam atque sequi dignissimos fugiat voluptate hic nihil.",
                    Finished_At = new DateTime(2019,8,20),
                    Performer_id = 2,
                    Project_id = 1,
                    State = 3
                },
                new TaskModel
                {
                    Id = 3,
                    Name = "dasd madp aksd apsp dsa",
                    Created_At = new DateTime(2019, 5, 19),
                    Description = "ihil repudiandae possimus eos ea nobis.\nEaque ipsam atque sequi dignissimos",
                    Finished_At = new DateTime(2020,12,1),
                    Performer_id = 3,
                    Project_id = 1,
                    State = 4
                },
                new TaskModel
                {
                    Id = 4,
                    Name = "131 312 4 014014 -1-4021412312 ",
                    Created_At = new DateTime(2019, 8, 19),
                    Description = "quae alias",
                    Finished_At = new DateTime(2019, 12,1),
                    Performer_id = 1,
                    Project_id = 1,
                    State = 2
                }
            };

            var taskStates = new List<TaskState>
            {
                new TaskState{Id = 1, Value = "Created"},
                new TaskState{Id = 2, Value = "Started"},
                new TaskState{Id = 3, Value = "Finished"},
                new TaskState{Id = 4, Value = "Canceled"}
            };

            var teams = new List<Team>()
            {
                new Team
                {
                    Id = 1,
                    Name = "labore",
                    Created_At = new DateTime(2019,6,19)
                },

                new Team
                {
                    Id = 2,
                    Name = "sunt",
                    Created_At = new DateTime(2019,6,19)
                }
            };

            var users = new List<User>
            {
                new User{Id = 1, First_Name = "Gayle", Last_Name = "Swift", Email = "Gayle_Swift98@yahoo.com", Birthday = new DateTime(2013,11,7), Registered_At = new DateTime(2019,6,17),Team_Id = 2},
                new User{Id = 2, First_Name = "Tad", Last_Name = "Polowski", Email = "Tad_Powlowski27@hotmail.com", Birthday = new DateTime(2017,10,11), Registered_At = new DateTime(2019,6,11),Team_Id = 2},
                new User{Id = 3, First_Name = "Letha", Last_Name = "Strosin", Email = "Letha.Strosin58@gmail.com", Birthday = new DateTime(2002,5,11), Registered_At = new DateTime(2019,6,17),Team_Id = 2},
                new User{Id = 4, First_Name = "Ivan", Last_Name = "Many", Email = "Letha@gmail.com", Birthday = new DateTime(2005,5,11), Registered_At = new DateTime(2019,2,10),Team_Id = 2}
            };

            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<TaskState>().HasData(taskStates);
            modelBuilder.Entity<TaskModel>().HasData(tasks);
            modelBuilder.Entity<Project>().HasData(projects);

            base.OnModelCreating(modelBuilder);
        }
    }
}
