﻿using Microsoft.EntityFrameworkCore;
using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Abstractions
{
    public interface IDbContext
    {
        DbSet<Project> Projects { get; set; }
        DbSet<TaskModel> TaskModels { get; set; }
        DbSet<TaskState> TaskStates { get; set; }
        DbSet<Team> Teams { get; set; }
        DbSet<User> Users { get; set; }

        int SaveChanges();
    }
}
