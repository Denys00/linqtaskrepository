﻿using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Abstractions
{
    public interface ITeamRepository
    {
        Task<Team> GetTeam(int id);
        Task<IEnumerable<Team>> GetTeams();
        Task<Team> Add(Team team);
        Task<Team> Update(Team team);
        Task<Team> Delete(int id);
    }
}
