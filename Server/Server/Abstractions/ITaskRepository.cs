﻿using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Abstractions
{
    public interface ITaskRepository
    {
        Task<TaskModel> GetTask(int id);
        Task<IEnumerable<TaskModel>> GetTasks();
        Task<TaskModel> Add(TaskModel task);
        Task<TaskModel> Update(TaskModel task);
        Task<TaskModel> Delete(int id);
    }
}
