﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Abstractions
{
    public interface IQueueService
    {
        void MessageRecived(object sender, BasicDeliverEventArgs args);
        bool PostValue(string message);
    }
}
