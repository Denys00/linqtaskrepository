﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Abstractions
{
    public interface IService<T>
    {
        Task<T> Get(int id);
        Task<IEnumerable<T>> GetAll();
        Task<T> Add(T taskState);
        Task<T> Update(T taskState);
        Task<T> Delete(int id);
    }
}
