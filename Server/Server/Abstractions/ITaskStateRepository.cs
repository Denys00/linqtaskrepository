﻿using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Abstractions
{
    public interface ITaskStateRepository
    {
        Task<TaskState> GetTaskState(int id);
        Task<IEnumerable<TaskState>> GetTaskStates();
        Task<TaskState> Add(TaskState taskState);
        Task<TaskState> Update(TaskState taskState);
        Task<TaskState> Delete(int id);
    }
}
