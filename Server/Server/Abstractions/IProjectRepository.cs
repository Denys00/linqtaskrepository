﻿using Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Abstractions
{
    public interface IProjectRepository
    {
        Task<Project> GetProject(int id);
        Task<IEnumerable<Project>> GetProjects();
        Task<Project> Add(Project project);
        Task<Project> Update(Project project);
        Task<Project> Delete(int id);
    }
}
