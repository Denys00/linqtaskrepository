﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace RequestProject.Tests
{
    [TestFixture]
    public class RequestSenderTests
    {
        [Test]
        public async Task When_Create_New_Team_Then_Succed()
        {
            //Arrange
            var sender = new RequestSender("https://localhost:44314/api/");

            //Act
            var result = await sender.PostTeam(new Team { Created_At = DateTime.Now, Name = "Test Team" });

            //Assert
            Assert.That(result.IsSuccessStatusCode);
        }

        [Test]
        public async Task When_Send_Delete_Not_Existing_User_Then_Null_Result()
        {
            //Arrange
            var sender = new RequestSender("https://localhost:44314/api/");

            //Act
            var result = await sender.DeleteUser(100);

            //Assert
            Assert.That(result.StatusCode.ToString, Is.EqualTo("NoContent"));
        }

        [Test]
        public async Task Test_When_Post_Project_Then_Success()
        {
            //Arrange
            var sender = new RequestSender("https://localhost:44314/api/");

            //Act
            var result = await sender.PostProject(new Project { Author_id = 1, Created_At = DateTime.Now, Deadline = DateTime.Now, Description = "Test", Name = "Test", Team_id = 1 });

            //Assert
            Assert.That(result.IsSuccessStatusCode);
        }

        [Test]
        public async Task When_Send_Delete_Existing_Task_Then_Success()
        {
            //Arrange
            var sender = new RequestSender("https://localhost:44314/api/");

            //Act
            var result = await sender.DeleteTask(9);

            //Assert
            Assert.That(result.IsSuccessStatusCode);
        }

        [Test]
        public async Task When_Queue_Manager_Get_Teams_Then_Not_Null()
        {
            //Arrange
            var sender = new RequestSender("https://localhost:44314/api/");
            var queue = new QueryManager(sender);

            //Act
            var result = await queue.GetTeams();

            //Assert
            Assert.NotNull(result);
        }

        [Test]
        public void When_Get_Tasks_Of_User_Then_Not_Exception()
        {
            var sender = new RequestSender("https://localhost:44314/api/");
            var manager = new QueryManager(sender);

            Assert.DoesNotThrowAsync(() => manager.GetTasks(2));
        }

        [Test]
        public async Task When_Call_Get_Finished_Tasks_Fot_Not_Existing_User_Then_Result_Empty()
        {
            var sender = new RequestSender("https://localhost:44314/api/");
            var manager = new QueryManager(sender);

            var result = await manager.GetFinishedTasks(100);

            Assert.IsEmpty(result);
        }
    }
}
