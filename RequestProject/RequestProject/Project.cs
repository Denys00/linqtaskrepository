﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RequestProject
{
    class Project
    { 
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Deadline { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }

        public int Author_id { get; set; }
        public int Team_id { get; set; }

        public override string ToString()
        {
            return $"{Id} | {Name} | {Description} ";
        }
    }
}
