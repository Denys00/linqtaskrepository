﻿using Microsoft.AspNetCore.SignalR.Client;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RequestProject
{
    class RequestSender
    {

        private HttpClient _client;
        private HubConnection _connection;

        //Initilize HttpClient
        public RequestSender(string basePath)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(basePath);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            Connect();
        }


        private async void Connect()
        {
            Console.WriteLine("Connected");
            _connection = new HubConnectionBuilder().WithUrl("https://localhost:44314/mainHub").Build();
            _connection.On<string>("GetNotification", on => Console.WriteLine(on));
            await _connection.StartAsync();
                    
        }

        public async Task CallMethod(string method)
        {
            await _connection.InvokeAsync(method);
        }

        public async Task StopConnection()
        {
            await _connection.StopAsync();
        }

        public async Task<HttpResponseMessage> PutTask(TaskModel task)
        {
            return await _client.PutAsJsonAsync($"tasks", task);
        }

        public async Task<HttpResponseMessage> DeleteTask(int id)
        {
            return await _client.DeleteAsync($"tasks/{id}");
        }

        public async Task<HttpResponseMessage> PostTeam(Team team)
        {
            return await _client.PostAsJsonAsync("teams", team);
        }

        //Get some Data From WebApi By url
        public async Task<string> GetData(string path)
        {
            HttpResponseMessage response = await _client.GetAsync(path);
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<IEnumerable<User>> GetUsers(string path)
        {
            var responseBody = await GetData(path);
            return JsonConvert.DeserializeObject<IEnumerable<User>>(responseBody);
        }

        public async Task<HttpResponseMessage> DeleteUser(int id)
        {
            return await _client.DeleteAsync($"users/{id}");
        }

        public async Task<Project> GetProject(string path, int id)
        {
            var responseBody = await GetData($"{path}/{id}");
            var project = JsonConvert.DeserializeObject<Project>(responseBody);
            return project;
        }

        public async Task<HttpResponseMessage> PostProject(Project project)
        {
            return await _client.PostAsJsonAsync("projects", project);
        }

        public async Task<User> GetUser(string path, int id)
        {
            var responseBody = await GetData($"{path}/{id}");
            var user = JsonConvert.DeserializeObject<User>(responseBody);
            return user;
        }

        public async Task<IEnumerable<Project>> GetProjects(string path)
        {
            var responseBody = await GetData(path);
            return JsonConvert.DeserializeObject<IEnumerable<Project>>(responseBody);
        }

        public async Task<IEnumerable<TaskModel>> GetTasks(string path)
        {
            var responseBody = await GetData(path);
            return JsonConvert.DeserializeObject<IEnumerable<TaskModel>>(responseBody);
        }

        public async Task<TaskModel> GetTask(string path, int id)
        {
            var responseBody = await GetData($"{path}/{id}");
            return JsonConvert.DeserializeObject<TaskModel>(responseBody);
        }

        public async Task<IEnumerable<TaskState>> GetTaskStates(string path)
        {
            var responseBody = await GetData(path);
            return JsonConvert.DeserializeObject<IEnumerable<TaskState>>(responseBody);
        }

        public async Task<IEnumerable<Team>> GetTeams(string path)
        {
            var responseBody = await GetData(path);
            return JsonConvert.DeserializeObject<IEnumerable<Team>>(responseBody);
        }
    }
}
