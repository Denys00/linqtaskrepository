﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RequestProject
{
    class QueryManager
    {
        private readonly RequestSender _sender;

        public QueryManager(RequestSender sender)
        {
            _sender = sender;            
        }

        //Get Dictionary where key is user projects and value is count of tasks in project
        public async Task<Dictionary<Project,int>> CountOfTasksQuery(int UserId)
        {
            var tasks = await _sender.GetTasks("tasks");
            var projects = await _sender.GetProjects("projects");

            var result = (from t in tasks
                          join p in projects on t.Project_id equals p.Id
                          where p.Author_id == UserId
                          group t by p into g
                          select new { Proj = g.Key, Count = g.Count() }).ToDictionary(g => g.Proj, c => c.Count);
        
            return result;
        }

        public async Task CallSenderMethod(string method)
        {
            await _sender.CallMethod(method);
        }

        //Get Tasks By User Id And Length of task should be less then 45
        public async Task<IEnumerable<TaskModel>> GetTasks(int Id)
        {
            var tasks = await _sender.GetTasks("tasks");
            var task = await Task.Run(() => tasks.Where(t => t.Performer_id == Id && t.Name.Length < 45));
            return task;
        }

        public async Task PutTask(TaskModel task)
        {
            await _sender.PutTask(task);
        }

        public async Task<IEnumerable<TaskModel>> GetTasks()
        {
            var tasks = await _sender.GetTasks("tasks");
            return tasks;
        }

        public async Task<IEnumerable<TaskState>> GetTaskStates()
        {
            var tasks = await _sender.GetTaskStates("taskstates");
            return tasks;
        }

        //Get typle of tasks of user that ended in 2019
        public async Task<IEnumerable<(int,string)>> GetFinishedTasks(int Id)
        {
            var tasks = await _sender.GetTasks("tasks");
            var taskState = await _sender.GetTaskStates("taskstates");

            var result = from task in tasks
                         where task.Performer_id == Id
                         where task.Finished_At.Year == 2019
                         select (id: task.Id, name: task.Name);

            
            return result;
        }

        //Get Team Of Users that older 12
        public async Task<IEnumerable<IGrouping<Team,User>>> GetTeams()
        {
            var users = await _sender.GetUsers("users");
            var teams = await _sender.GetTeams("teams");
            var result = from user in users
                         where DateTime.Now.Year - user.Birthday.Year > 12
                         orderby user.Registered_At descending
                         join t in teams on user.Team_Id equals t.Id
                         group user by t;

            return result;
        }

        public async Task<Dictionary<User, IOrderedEnumerable<TaskModel>>> GetSortedUsersAndTasks()
        {
            var users = await _sender.GetUsers("users");
            var tasks = await _sender.GetTasks("tasks");

            var result = (from user in users
                          orderby user.First_Name 
                          join t in tasks on user.Id equals t.Performer_id
                          group t by user into g
                          select new { User = g.Key, Tasks = g.OrderByDescending(x => x.Name.Length) }).ToDictionary(u => u.User, t => t.Tasks);

            return result;
        }

        public async Task<User> GetUser(int id)
        {
            User user = await _sender.GetUser("users", id);
            return user;
        }

        public async Task<KeyValuePair<Project,int>> LastProject(int id)
        {
            var projects = await CountOfTasksQuery(id);
            var project = projects.OrderByDescending(x => x.Key.Created_At).First();
            
            return project;
        }

        public async Task<int> UnfinishedTaskCount(int id)
        {
            var tasks = await _sender.GetTasks("tasks");
            var states = await _sender.GetTaskStates("taskstates");

            var count = (from task in tasks
                         where task.Performer_id == id
                         join state in states on task.State equals state.Id
                         where state.Value != "Finished"
                         select task).Count();

            return count;
        }

        public async Task<TaskModel> LongestTask(int id)
        {
            var tasks = await _sender.GetTasks("tasks");

            var task = tasks.Where(x => x.Performer_id == id).OrderByDescending(x => x.Finished_At - x.Created_At).First();

            return task;
        }

        public async Task<Project> GetProject(int id)
        {
            Project project = await _sender.GetProject("projects", id);
            return project;
        }

        public async Task<TaskModel> LongestTaskDesc(int id)
        {
            var tasks = await _sender.GetTasks("tasks");

            var task = tasks.Where(t => t.Project_id == id).OrderByDescending(x => x.Description.Length).First();

            return task;
        }

        public async Task<TaskModel> ShortestTaskName(int id)
        {
            var tasks = await _sender.GetTasks("tasks");

            var task = tasks.Where(t => t.Project_id == id).OrderBy(x => x.Name.Length).First();

            return task;
        }

        //Not sure is this right 
        public async Task<int> UsersCount(int id)
        {
            var project = await GetProject(id);
            var users = await _sender.GetUsers("users");
            var tasks = await _sender.GetTasks("tasks");

            var count = (from user in users
                         where user.Team_Id == project.Team_id
                         where (from task in tasks
                                where task.Project_id == project.Id
                                select task).Count() < 3 || project.Description.Length > 25
                         select user).Count();
                         


            return count;
        }

    }
}
