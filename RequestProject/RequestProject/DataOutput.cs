﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace RequestProject
{
    class DataOutput
    {
        private QueryManager _manager;

        public DataOutput(QueryManager manager)
        {
            _manager = manager;
        }

        public async Task PrintDictionary(int Id)
        {
            var dict = await _manager.CountOfTasksQuery(Id);
            foreach (var item in dict)
            {
                Console.WriteLine($"{item.Key.ToString()} || Count Of Tasks || {item.Value}");
            }
        }

        public async Task PrintTasks(int Id)
        {
            var tasks = await _manager.GetTasks(Id);
            foreach (var task in tasks)
            {
                Console.WriteLine(task?.ToString());
            }
        }

        public async Task PrintTyple(int Id)
        {
            var typle = await _manager.GetFinishedTasks(Id);
            foreach (var t in typle)
            {
                Console.WriteLine($"{t.Item1 } | {t.Item2}");
            }
        }

        public async Task PrintTeams()
        {
            var users = await _manager.GetTeams();
            foreach (var team in users)
            {
                Console.WriteLine($"Team ID: {team.Key.Id} | Name: {team.Key.Name}");
                foreach (var user in team)
                {
                    Console.WriteLine($"{user.ToString()}");
                }
            }
        }

        public async Task PrintUsers()
        {
            var keyValuePairs = await _manager.GetSortedUsersAndTasks();
            foreach (var item in keyValuePairs)
            {
                Console.WriteLine(item.Key.ToString());
                foreach (var task in item.Value)
                {
                    Console.WriteLine(task.ToString());
                }
                Console.WriteLine("==============================================");
            }
        }

        public async Task PrintUserStruct(int id)
        {
            User user = await _manager.GetUser(id);
            Console.WriteLine(user.ToString());
            var lastProject = await _manager.LastProject(id);
            Console.WriteLine($"Last Project: {lastProject.Key.ToString()}");
            Console.WriteLine($"Count Of Tasks In Last Project: {lastProject.Value}");
            Console.WriteLine($"Count Of Unfinished Tasks: {_manager.UnfinishedTaskCount(id)}");
            Console.WriteLine($"Longest Task: {_manager.LongestTask(id).ToString()}");
        }

        public async Task PrintProjectStruct(int id)
        {
            Project project = await _manager.GetProject(id);
            Console.WriteLine(project.ToString());
            Console.WriteLine($"Longest task by description: {_manager.LongestTaskDesc(id).ToString()}");
            Console.WriteLine($"Shortest task by name: {_manager.ShortestTaskName(id).ToString()}");
            Console.WriteLine($"Count of users: {_manager.UsersCount(id)}");
        }

        public void CleanConsole()
        {
            Console.WriteLine("Press any button...");
            Console.ReadKey();
            Console.Clear();
        }

        public int ReadId(string message)
        {
            int id = -1;
            Console.WriteLine(message);
            try
            {
                id = Convert.ToInt32(Console.ReadLine());
            }
            catch(Exception e)
            {
                throw new Exception("Wrong Input");
            }

            return id;
        }

        public async Task RunFileReciever()
        {
            string what = "1";
            Console.WriteLine("Write '0' to stop procces\n'1' To output file information");
            while(what != "0")
            {
                what = Console.ReadLine();
                if(what == "1")
                {
                    await _manager.CallSenderMethod("GetSortedFile");
                }
            }
        }

        private async Task<int> ChangeToFinish()
        {
            var tasks = await _manager.GetTasks();
            var taskStates = await _manager.GetTaskStates();
            var stateId = taskStates.First(x => x.Value == "Finished").Id;
            Random rnd = new Random();
            int id = rnd.Next(tasks.Max(x => x.Id));
            var task = tasks.First(x => x.Id == id);
            task.State = stateId;
            await _manager.PutTask(task);
            return task.Id;
        }

        private void SetTimer(int deley)
        {
            // Create a timer with a two second interval.
            var aTimer = new System.Timers.Timer(deley);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private async void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            try
            {
                var id = await ChangeToFinish();
                Console.WriteLine(id);
            }
            catch (AggregateException ae)
            {
                foreach (var ex in ae.InnerExceptions)
                {
                    // Handle the custom exception.
                    if (ex is Exception)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    
                }
            }
            catch(Exception exp)
            {
                Console.WriteLine(exp.Message);
            }
        }

        public async Task Run()
        {
            int id = 0;
            try
            {
                SetTimer(1000);
                Console.WriteLine("---|Count Of Tasks In Project Of User|---");
                id = ReadId("Input User Id: ");
                PrintDictionary(id);
                CleanConsole();
                Console.WriteLine("---|Tasks Of User|---");
                id = ReadId("Input User Id: ");
                PrintTasks(id);
                CleanConsole();
                Console.WriteLine("---|Finished In 2019 Tasks Of User|---");
                id = ReadId("Input User Id: ");
                PrintTyple(id);
                CleanConsole();
                Console.WriteLine("---|Teams Of Users Older Than 12|---");
                PrintTeams();
                CleanConsole();
                Console.WriteLine("---|Users And Their Tasks|---");
                PrintUsers();
                CleanConsole();
                Console.WriteLine("---|User Struct|---");
                id = ReadId("Input User Id: ");
                PrintUserStruct(id);
                CleanConsole();
                Console.WriteLine("---|Project Struct|---");
                id = ReadId("Input Project Id: ");
                PrintProjectStruct(id);
                CleanConsole();
            }
            catch(Exception e)
            {
                Console.WriteLine("Ooooopssss!!!! Something Wrong(");
            }
        }
    }
}
