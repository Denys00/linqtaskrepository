﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RequestProject
{
    class Program
    {
        static void PrintDictionary(Dictionary<Project,int> vs)
        {
            foreach(var item in vs)
            {
                Console.WriteLine($"{item.Key.ToString()} | {item.Value}");
            }
        }

        static void PrintTasks(IEnumerable<TaskModel> tasks)
        {
            foreach(var task in tasks)
            {
                Console.WriteLine(task.ToString());
            }
        }

        static void PrintTyple(IEnumerable<(int,string)> typle)
        {
            foreach(var t in typle)
            {
                Console.WriteLine($"{t.Item1 } | {t.Item2}");
            }
        }

        static void PrintTeams(IEnumerable<IGrouping<Team, User>> users)
        {
            foreach(var team in users)
            {
                Console.WriteLine($"Team ID: {team.Key.Id} | Name: {team.Key.Name}");
                foreach(var user in team)
                {
                    Console.WriteLine($"{user.ToString()}");
                }
            }
        }

        static void PrintUsers(Dictionary<User, IOrderedEnumerable<TaskModel>> keyValuePairs)
        {
            foreach(var item in keyValuePairs)
            {
                Console.WriteLine(item.Key.ToString());
                foreach (var task in item.Value)
                {
                    Console.WriteLine(task.ToString());
                }
                Console.WriteLine("==============================================");
            }
        }

       

        static void Main(string[] args)
        {
            RequestSender sender = new RequestSender("https://localhost:44314/api/");
            QueryManager manager = new QueryManager(sender);
            
            DataOutput output = new DataOutput(manager);

            //var res = sender.PostTeam(new Team { Created_At = DateTime.Now, Name = "Test" }).Result;
            // var res = sender.DeleteUser(5).Result;

            //output.RunFileReciever();
            output.Run();

            //output.PrintDictionary(37);
            //output.PrintTasks(1);
            //output.PrintTyple(1);
            //output.PrintTeams();
            //output.PrintUsers();
            
            //Console.Read();
            //output.PrintUserStruct(9);
            //output.PrintProjectStruct(7);

            //PrintDictionary(manager.CountOfTasksQuery(2));
            //PrintTasks(manager.GetTasks(1));
            //PrintTyple(manager.GetFinishedTasks(15));
            //PrintTeams(manager.GetTeams());
            //sender.StopConnection();
            //PrintUsers(manager.GetSortedUsersAndTasks());
        }
    }
}
