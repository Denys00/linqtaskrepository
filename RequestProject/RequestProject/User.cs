﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RequestProject
{
    class User
    {
        public int Id { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime Registered_At { get; set; }


        public int? Team_Id { get; set; }

        public override string ToString()
        {
            return $"Name: {First_Name} {Last_Name} | Birthday: {Birthday} | Registered At: {Registered_At}";
        }
    }
}
