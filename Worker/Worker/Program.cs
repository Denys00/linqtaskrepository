﻿using System;

namespace Worker
{
    class Program
    {
        static void Main(string[] args)
        {
            var s = new MessageService();
            s.Run();
        }
    }
}
