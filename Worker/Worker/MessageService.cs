﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Worker
{
    class MessageService : IDisposable
    {

        private EventingBasicConsumer _consumer;
        private IModel _channel;
        private IConnection _connection;


        public MessageService()
        {

        }

        public void Run()
        {
            Console.WriteLine("Service Run");
            Configure();
        }

        private void Configure()
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672")
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare("Exchange", ExchangeType.Direct);
            _channel.QueueDeclare("Queue",
                durable: true,
                exclusive: false,
                autoDelete: false
                );

            _channel.QueueBind(queue: "Queue", exchange: "Exchange", routingKey: "key");

            _consumer = new EventingBasicConsumer(_channel);

            _consumer.Received += MessageRecived;

            _channel.BasicConsume(queue: "Queue",
                autoAck: false,
                consumer: _consumer);
        }

        private void MessageRecived(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body;
            var message = Encoding.UTF8.GetString(body);
            try
            {
                ToFile(message);
                SendBack("Successfully received");
            }
            catch(Exception e)
            {
                SendBack($"Failed: {e.Message}");
            }
            _channel.BasicAck(args.DeliveryTag, false);
            
        }


        private bool SendBack(string message)
        {

            _channel.ExchangeDeclare("BackExchange", ExchangeType.Direct);

            var body = Encoding.UTF8.GetBytes(message);

            _channel.BasicPublish(exchange: "BackExchange",
                routingKey: "backKey",
                basicProperties: null,
                body: body);

            return true;

        }

        private void ToFile(string message)
        {
            using (StreamWriter sw = File.AppendText(@"C:\MyProjects\binary-studio\linqtaskrepository\Worker\Worker\Info.txt"))
            {
                sw.WriteLine($"{message},{DateTime.Now.ToString()}");
            }
        }

        public void Dispose()
        {
            _channel?.Dispose();
            _connection?.Dispose();
        }
    }
}
